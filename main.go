package main

import (
	"flag"
	"fmt"
	"log"
	"time"

	"github.com/google/gopacket"
	"github.com/google/gopacket/layers"
	"github.com/google/gopacket/pcap"
)

var (
	snapshotLen    int32 = 1024
	promiscuous          = false
	timeout              = 30 * time.Second
	filterProtocol       = "udp"
)

var (
	interfaceFlag  = flag.String("interface", "lo", "interface to capture")
	filterPortFlag = flag.String("port", "1813", "port to filter packets")
	tcpFlag        = flag.Bool("tcp", false, "filter tcp port")
)

func main() {

	flag.Parse()

	if *tcpFlag {
		filterProtocol = "tcp"
	}
	fs := FilterSettings{*interfaceFlag, filterProtocol, *filterPortFlag}

	// Open device
	handle, err := pcap.OpenLive(fs.deviceInterface, snapshotLen, promiscuous, timeout)
	if err != nil {
		log.Fatal(err)
	}
	defer handle.Close()

	// Start Listener
	packets, err := listenOnInterface(handle, fs)

	if err != nil {
		log.Fatal("error on create packet", err)
	}

	// listen on packets channel
	if packets != nil {
		for packet := range packets {
			printAllInfo(packet)
		}
	}
}

// FilterSettings is custom filter configurations
type FilterSettings struct {
	deviceInterface string
	protocol        string
	port            string
}

// listen on interface with custom filter settings
func listenOnInterface(handle *pcap.Handle, fs FilterSettings) (chan gopacket.Packet, error) {

	//Set filter
	var filter = fmt.Sprintf("%s and port %s", fs.protocol, fs.port)
	if err := handle.SetBPFFilter(filter); err != nil {
		return nil, err
	}
	log.Printf("Only capturing %s port %s packets on %s.\n\n", fs.protocol, fs.port, fs.deviceInterface)

	packetSource := gopacket.NewPacketSource(handle, handle.LinkType())

	packets := packetSource.Packets()

	return packets, nil
}

func printAllInfo(packet gopacket.Packet) {

	parsed, foundLayerTypes, err := parseLayersInfo(packet)

	if err != nil {
		log.Println("Error in parse packet: ", err)
	}

	fmt.Printf("Found layers: %s \n\n", foundLayerTypes)

	for _, layerType := range foundLayerTypes {

		if layerType == layers.LayerTypeUDP {
			printUDPLayerPacketInfo(parsed.udpLayer)
		}
		if layerType == layers.LayerTypeTCP {
			printTCPLayerPacketInfo(parsed.tcpLayer)
		}
		if layerType == layers.LayerTypeIPv4 {
			printIPv4LayerPacketInfo(parsed.ipLayer)
		}
		if layerType == layers.LayerTypeEthernet {
			printEthernetLayerPacketInfo(parsed.ethLayer)
		}
	}

	// Check for errors
	if err := packet.ErrorLayer(); err != nil {
		log.Println("Error decoding some part of the packet:", err)
	}

	fmt.Println("-----------------------------")
}

// ParsedPacket is layer separated packed type
type ParsedPacket struct {
	ethLayer     layers.Ethernet
	ipLayer      layers.IPv4
	udpLayer     layers.UDP
	tcpLayer     layers.TCP
	payloadLayer gopacket.Payload
}

// parseLayersInfo get packet and parse it to layer separated object
func parseLayersInfo(packet gopacket.Packet) (ParsedPacket, []gopacket.LayerType, error) {

	var parsed ParsedPacket

	parser := gopacket.NewDecodingLayerParser(
		layers.LayerTypeEthernet,
		&parsed.ethLayer,
		&parsed.ipLayer,
		&parsed.tcpLayer,
		&parsed.udpLayer,
		&parsed.payloadLayer,
	)
	foundLayerTypes := []gopacket.LayerType{}

	err := parser.DecodeLayers(packet.Data(), &foundLayerTypes)
	if err != nil {
		return parsed, foundLayerTypes, err
	}

	return parsed, foundLayerTypes, nil
}

func printEthernetLayerPacketInfo(ethernetPacket layers.Ethernet) {
	fmt.Println("Ethernet layer detected.")
	fmt.Println("\tSource MAC: ", ethernetPacket.SrcMAC)
	fmt.Println("\tDestination MAC: ", ethernetPacket.DstMAC)

	// Ethernet type is typically IPv4 but could be ARP or other
	fmt.Println("\tEthernet type: ", ethernetPacket.EthernetType)
	fmt.Println()

}

func printIPv4LayerPacketInfo(ipPacket layers.IPv4) {
	fmt.Println("IPv4 layer detected.")

	// IP layer variables:
	// Version (Either 4 or 6)
	// IHL (IP Header Length in 32-bit words)
	// TOS, Length, Id, Flags, FragOffset, TTL, Protocol (TCP,UDP,...),
	// Checksum, SrcIP, DstIP
	fmt.Printf("\tFrom %s to %s\n", ipPacket.SrcIP, ipPacket.DstIP)
	fmt.Println("\tProtocol: ", ipPacket.Protocol)
	fmt.Println()

}

func printTCPLayerPacketInfo(tcpPacket layers.TCP) {
	fmt.Println("TCP layer detected.")

	// TCP layer variables:
	// SrcPort, DstPort, Seq, Ack, DataOffset, Window, Checksum, Urgent
	// Bool flags: FIN, SYN, RST, PSH, ACK, URG, ECE, CWR, NS
	fmt.Printf("\tFrom port %d to %d\n", tcpPacket.SrcPort, tcpPacket.DstPort)
	fmt.Println("\tSequence number: ", tcpPacket.Seq)
	fmt.Println("\tTCP layer payload: ", string(tcpPacket.LayerPayload()))
	fmt.Println()

}

func printUDPLayerPacketInfo(udpPacket layers.UDP) {
	fmt.Println("UDP layer detected.")

	fmt.Printf("\tFrom port %d to %d\n", udpPacket.SrcPort, udpPacket.DstPort)
	fmt.Println("\tUDP layer payload: ", string(udpPacket.LayerPayload()))
	fmt.Println()

}
