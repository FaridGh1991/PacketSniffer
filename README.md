# PacketSniffer

Simple packet sniffer that use gopacket library to print packet layers information.

run options:

`--interface <interfacename>` to change interface(default is `lo`) 

`--port <port number>` to change port(default is `1813`)

`-tcp` to change protocol to tcp(default is `UDP`) 
